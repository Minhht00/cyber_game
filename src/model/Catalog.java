/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author lexuanmuoi
 */
public class Catalog {
    public int id,status,createBy,updateBy;
    String name;
    public Catalog(){};
    public Catalog(int id, String name,int status,int createBy,int updateBy){
    this.id = id;
    this.name = name;
    this.status = status;
    this.createBy = createBy;
    this.updateBy = updateBy;
};
    public int getId(){
        return this.id;
    }
    public void setId(int id){
        this.id = id;
    }
    public int getStatus(){
        return this.status ;
    }
    public void setStatus(int status){
        this.status = status;
    }
    public int getCreateBy(){
        return this.createBy ;
    }
    public void setCreateBy(int createBy){
        this.createBy = createBy;
    }
    public int getUpdateBy(){
        return this.updateBy ;
    }
    public void setUpdate(int updateBy){
        this.updateBy = updateBy;
    }
}
