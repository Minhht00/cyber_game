/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author lexuanmuoi
 */
public class Sales {
    public int id,amount,status,createBy,updateBy;
    public Sales(){};
    public Sales(int id, int amount,int status,int createBy,int updateBy){
    this.id = id;
    this.amount = amount;
    this.status = status;
    this.createBy = createBy;
    this.updateBy = updateBy;
};
    public int getId(){
        return this.id;
    }
    public void setId(int id){
        this.id = id;
    }
    public int getAmount(){
        return this.amount;
    }
    public void setAmount(int amount){
        this.amount = amount;
    }
    public int getStatus(){
        return this.status ;
    }
    public void setStatus(int status){
        this.status = status;
    }
    public int getCreateBy(){
        return this.createBy ;
    }
    public void setCreateBy(int createBy){
        this.createBy = createBy;
    }
    public int getUpdateBy(){
        return this.updateBy ;
    }
    public void setUpdate(int updateBy){
        this.updateBy = updateBy;
    }
}
